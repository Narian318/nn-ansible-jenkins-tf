terraform {
  backend "s3" {
    bucket = "nn-anible-jenkins-tf" # Replace with your actual S3 bucket name
    key    = "Gitlab/terraform.tfstate"
    region = "ap-south-1"
  }
}
